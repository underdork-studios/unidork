Unidork is a collection of extensions, utility methods, useful subsystems and whatnot that I started putting together during my time as a senior developer at Estoty and continued while leading a team at SoupInItGames. Some of it is still WIP - and I swear I will finish those systems someday - but most of it has been tried and tested in real released projects. It was mostly used to make mobile games but by no means is limited to them.

Feel free to use anything you find here under blah-blah-blah license (whichever one makes me not guilty for your bugs!).

