using Cysharp.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace Unidork.SceneManagement
{
	/// <summary>
	/// Simple main scene loader that loads game's main scene from an
	/// Addressable asset and unloads the splash scene upon completion.
	/// </summary>
	public class MainSceneLoader : MonoBehaviour
	{
		#region Fields

		/// <summary>
		/// Main scene Addressable asset reference.
		/// </summary>
		[Tooltip("Main scene Addressable asset reference.")]
		[SerializeField]
		private AssetReference mainSceneAssetReference = null;

		#endregion

		#region Init

		private void Start() => LoadMainScene();

		#endregion

		#region Load

		/// <summary>
		/// Starts the asynchronous load of the main scene.
		/// </summary>
		private void LoadMainScene()
		{
			SplashSceneLoader.AlreadyLoadedSplash = true;

			_ = SceneManager.LoadSceneAsync(mainSceneAssetReference, LoadSceneMode.Additive, activateOnLoad: true, setAsActiveScene: true, sceneLoadCallback: null);
		}

		#endregion

		#region Unload
		
		/// <summary>
		/// Starts the asynchronous unload of the splash scene.
		/// </summary>
		public void StartSplashSceneUnload(float delayInSeconds) => UnloadSplashSceneAsync(delayInSeconds).Forget();

		/// <summary>
		/// Unloads the splash scene asynchronously after a delay.
		/// </summary>
		/// <returns>
		/// <see cref="UniTaskVoid"/>.
		/// </returns>
		private async UniTaskVoid UnloadSplashSceneAsync(float delay)
		{
			await UniTask.Delay(new TimeSpan(0, 0, 0, 0, (int)(delay * 1000f)));
			await UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(0);
		}

		#endregion
	}
}