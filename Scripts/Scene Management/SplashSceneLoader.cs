﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Unidork.SceneManagement
{
    /// <summary>
    /// Utility script that can be used to loads the splash scene to properly initialize the game when it's run in the editor.
    /// </summary>
    public class SplashSceneLoader : MonoBehaviour
    {        
        #region Properties

        public static bool AlreadyLoadedSplash { get; set; }

        public static AssetReference OverrideSceneAssetReference { get; private set; }

        #endregion

        #region Fields

        [Tooltip("Override scene to load when starting the game in the editor from the scene where this component is located." +
                 "When left empty, default scene is loaded instead.")]
        [SerializeField]
        private AssetReference overrideSceneAssetReference = null;

        #endregion

        #region Load

        private void Awake()
        {
#if UNITY_EDITOR
            if (!AlreadyLoadedSplash)
            {
                OverrideSceneAssetReference = overrideSceneAssetReference;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Splash");
            }
#endif
        }

        #endregion
    } 
}