#if ADDRESSABLES

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

using static UnityEngine.AddressableAssets.Addressables;

namespace Unidork.AddressableAssetsUtility
{
	/// <summary>
	/// Utility methods for Unity's Addressables system.
	/// </summary>
	public static class AddressablesUtility
	{
		/// <summary>
		/// Loads an Addressable game object and gets a component of specified type on that game object.
		/// </summary>
		/// <param name="assetReference">Addressable asset reference.</param>
		/// <typeparam name="T">Type of component to get.</typeparam>
		/// <returns>
		/// An async operation handle that stores a component of type <see cref="T"/> as its result.
		/// </returns>
		public static AsyncOperationHandle<T> LoadComponentAsync<T>(AssetReference assetReference) where T : Component
		{
			var componentAsyncLoadOperation = new ComponentAsyncLoadOperation<T>(assetReference);
			AsyncOperationHandle<T> loadHandle = ResourceManager.StartOperation(componentAsyncLoadOperation, default);
			return loadHandle;
		}
	}
}

#endif