﻿using Unidork.Events;
using UnityEngine;

namespace Unidork.Utility
{
    public class GameEventRaiser : MonoBehaviour
    {
        [SerializeField] private GameEvent eventToRaise = null;

        public void RaiseEvent() => eventToRaise.Raise();
    }
}