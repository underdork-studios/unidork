﻿using Sirenix.OdinInspector.Editor;

namespace Unidork.Events
{
	using UnityEditor;
	using UnityEngine;

	[CanEditMultipleObjects]
	[CustomEditor(typeof(GameEvent))]
	public class GameEventEditor : OdinEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			var @event = (GameEvent)target;

			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("RAISE"))
			{
				@event.Raise();
			}

			GUI.enabled = true;

			if (GUILayout.Button("COPY STREAM NAME"))
			{
				GUIUtility.systemCopyBuffer = @event.Name;
			}

			GUILayout.Label("DEBUG", EditorStyles.boldLabel);

			if (GUILayout.Button("Find All References"))
			{
				GameEventReferenceWindow.ShowWindow(@event);				
			}
		}
	}
}