using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using Unidork.Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Unidork.LootSystem
{
	/// <summary>
	/// Table storing loot data used for random weighted selection. Data is separated into groups, each with its own type.
	/// Groups also have have weights of their own so we are able to use random weighted selection on group level first
	/// (for example, to select between armor, weapon, or gold) and then on loot objects in the group itself (to pick a specific armor/weapon/gold item
	/// based on its weight/rarity).
	/// </summary>
	/// <typeparam name="TLootGroupType">Type of loot group.</typeparam>
	/// <typeparam name="TLoot">Loot.</typeparam>
	/// <typeparam name="TLootObjectType">Type of loot object.</typeparam>
	/// <typeparam name="TRarity">Loot rarity.</typeparam>
	public class WeightedLootTable<TLootGroupType, TLoot, TLootObjectType, TRarity> : ScriptableObject where TLootGroupType : Enum
																								 where TLoot:WeightedLoot<TLootObjectType, TRarity>
																								 where TRarity : Enum
	{
		#region Fields

		/// <summary>
		/// Groups in this table.
		/// </summary>
		[Tooltip("Groups in this table.")]
		[SerializeField]
		private List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>> groups = 
			new List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>>();

		/// <summary>
		/// Total weight of all groups in this table.
		/// </summary>
		[SerializeField, ReadOnly]
		private float totalWeight;
		
		/// <summary>
		/// Have the weights for this loot table been computed?
		/// </summary>
		private bool weightsComputed;

		/// <summary>
		/// Array storing all loot group type values.
		/// </summary>
		private static TLootGroupType[] allLootGroupTypes;
		
		/// <summary>
		/// Array storing all rarity values.
		/// </summary>
		private static TRarity[] allRarities;

		#endregion

		#region Loot

		/// <summary>
		/// Gets a random weighted loot object. Includes/excluded object groups and rarities based on passed optional arguments.
		/// </summary>
		/// <param name="groupTypesToInclude">Type of groups to include.</param>
		/// <param name="groupTypesToExclude">Type of groups to exclude.</param>
		/// <param name="raritiesToInclude">Type of rarities to include.</param>
		/// <param name="raritiesToExclude">Types of rarities to exclude.</param>
		/// <returns>
		/// An instance of <see cref="TLootObjectType"/> or <see cref="TLootObjectType"/> default value if no loot data exists
		/// or no loot data matches the specified selection parameters.
		/// </returns>
		public TLootObjectType GetLoot(List<TLootGroupType> groupTypesToInclude = null, List<TLootGroupType> groupTypesToExclude = null, 
		                               List<TRarity> raritiesToInclude = null, List<TRarity> raritiesToExclude = null)
		{
			if (groups.IsNullOrEmpty())
			{
				Debug.Log("Loot groups are null or empty!");
				return default;
			}

			WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity> randomGroup = null;

			List<TLootGroupType> allowedLootGroupTypes = CreateAllowedLootGroupList(groupTypesToInclude, groupTypesToExclude);

			if (allowedLootGroupTypes.IsEmpty())
			{
				Debug.LogError($"Allowed loot group type list is empty!");
				return default;
			}

			if (allowedLootGroupTypes.Count == allLootGroupTypes.Length)
			{
				ComputeGroupWeights(groups, out float _);
				randomGroup = GetWeightedRandomGroup(groups, totalWeight);
			}
			else
			{
				var groupsOfTypes = GetGroupsOfTypes(allowedLootGroupTypes);
				
				if (groupsOfTypes.IsEmpty())
				{
					Debug.LogWarning("Failed to find any loot groups matching the passed types!");
					return default;
				}

				ComputeGroupWeights(groupsOfTypes, out float groupTotalWeight);
				randomGroup = GetWeightedRandomGroup(groupsOfTypes, groupTotalWeight);
			}

			return randomGroup.GetLootByRarity(raritiesToInclude, raritiesToExclude);
		}

		/// <summary>
		/// Creates a list of allowed loot groups.
		/// </summary>
		/// <param name="groupTypesToInclude">Group types to include.</param>
		/// <param name="groupTypesToExclude">Group types to exclude.</param>
		/// <returns>
		/// A list of <see cref="TLootGroupType"/> based on passed arguments.
		/// </returns>
		private List<TLootGroupType> CreateAllowedLootGroupList(List<TLootGroupType> groupTypesToInclude = null, List<TLootGroupType> groupTypesToExclude = null)
		{
			List<TLootGroupType> allowedLootGroupTypes = new List<TLootGroupType>((groupTypesToInclude.IsNullOrEmpty() ? allLootGroupTypes : groupTypesToInclude)!);

			if (groupTypesToExclude != null)
			{
				allowedLootGroupTypes = allowedLootGroupTypes.FindAll(type => !groupTypesToExclude.Contains(type));	
			}

			return allowedLootGroupTypes;
		}

		/// <summary>
		/// Gets a weighted random group from a list of passed groups.
		/// </summary>
		/// <param name="weightedGroups">List of groups to choose from.</param>
		/// <param name="groupTotalWeight">Total weight of loot objects in this group.</param>
		/// <returns>
		/// A weighted loot group.
		/// </returns>
		private WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity> GetWeightedRandomGroup(
			List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>> weightedGroups, float groupTotalWeight)
		{
			float index = Random.Range(0, groupTotalWeight);
 
			foreach (var group in weightedGroups)
			{
				if ((index > group.RangeFrom) && (index < group.RangeTo))
				{
					return group;
				}
			}

			Debug.LogError(("Failed to get weighted group!"));
			return groups.First();
		}
		
		/// <summary>
		/// Gets all groups that match the passed group types.
		/// </summary>
		/// <param name="groupTypes">Group types to include.</param>
		/// <returns>
		/// A list of weighted groups that match the condition.
		/// </returns>
		private List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>> GetGroupsOfTypes(List<TLootGroupType> groupTypes)
		{
			var groupsOfTypes = new List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>>();

			foreach (var group in groups)
			{
				if (!groupTypes.Contains(group.Type))
				{
					continue;
				}
				
				groupsOfTypes.Add(group);
			}
			
			return groupsOfTypes;
		}

		#endregion

		#region Weights

		/// <summary>
		/// Computes the weights for all groups in this table and their content.
		/// </summary>
		[Button("COMPUTE WEIGHTS", ButtonSizes.Large), GUIColor(0f, 1f, 0f)]
		public void ComputeWeights()
		{
			if (groups.IsNullOrEmpty())
			{
				return;
			}

			ComputeGroupWeights(groups, out float totalGroupWeight);

			totalWeight = totalGroupWeight;

			weightsComputed = true;
		}

		/// <summary>
		/// Computes weights in the passed list of groups.
		/// </summary>
		/// <param name="groups">Group list.</param>
		private void ComputeGroupWeights(List<WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>> groups, out float totalWeight)
		{
			totalWeight = 0f;
			
			if (groups.IsNullOrEmpty())
			{
				return;
			}

			foreach(var group in groups)
			{
				if(group.Weight >= 0f)
				{
					group.RangeFrom = totalWeight;
					totalWeight += group.Weight;	
					group.RangeTo = totalWeight;
				} 
				else 
				{
					group.Weight =  0f;						
				}
			}
			
			foreach(var group in groups)
			{
				group.SelectionChance = group.Weight / totalWeight * 100f;
				group.ComputeWeights();
			}
		}

		#endregion

		#region Enable

		private void OnEnable()
		{
			if (!allLootGroupTypes.IsNullOrEmpty())
			{
				return;
			}
			
			Array groupTypeArray = Enum.GetValues(typeof(TLootGroupType));
			allLootGroupTypes = new TLootGroupType[groupTypeArray.Length];
			groupTypeArray.CopyTo(allLootGroupTypes, 0);
			
			Array rarityArray = Enum.GetValues(typeof(TRarity));
			allRarities = new TRarity[rarityArray.Length];
			rarityArray.CopyTo(allRarities, 0);
			
			WeightedLootGroup<TLootGroupType, TLoot, TLootObjectType, TRarity>.InitRaritiesArray(allRarities);
		}

		#endregion
	}
}