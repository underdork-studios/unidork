using System.Collections.Generic;
using Sirenix.OdinInspector;
using Unidork.Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Unidork.LootSystem
{
	/// <summary>
	/// Collection of weighted loot objects. Groups can separate objects by type and have weights of their own
	/// so we are able to use random weighted selection on group level first (for example, to select between armor, weapon, or gold)
	/// and then on loot objects in the group itself (to pick a specific armor/weapon/gold item based on its weight/rarity).
	/// </summary>
	/// <typeparam name="TLootGroupType">Type of loot group.</typeparam>
	/// <typeparam name="TLoot">Object that stores loot data.</typeparam>
	/// <typeparam name="TLootObject">Actual loot object.</typeparam>
	/// <typeparam name="TRarity">Loot rarity.</typeparam>
	[System.Serializable]
	public class WeightedLootGroup<TLootGroupType, TLoot, TLootObject, TRarity> : WeightedLoot<List<TLoot>, TRarity> where TLootGroupType : System.Enum
																														 where TLoot: WeightedLoot<TLootObject, TRarity>
																														 where TRarity : System.Enum
	{
		#region Properties

		/// <summary>
		/// Type of this loot group.
		/// </summary>
		/// <value>
		/// Gets the value of the field type.
		/// </value>
		public TLootGroupType Type => type;

		#endregion
		
		#region Fields

		/// <summary>
		/// Type of this loot group.
		/// </summary>
		[Tooltip("Type of this loot group.")]
		[PropertyOrder(0)]
		[SerializeField]
		private TLootGroupType type = default;

		/// <summary>
		/// Have the weights been computed for this group?
		/// </summary>
		private bool weightsComputed;
		
		/// <summary>
		/// Total weight of all loot objects in this group.
		/// </summary>
		private float totalLootWeight;

		/// <summary>
		/// Array storing all rarity values.
		/// </summary>
		private static TRarity[] AllRarities;

		#endregion

		#region Init

		/// <summary>
		/// Initializes the all rarities array.
		/// </summary>
		/// <param name="allRarities">Array to initialize with.</param>
		public static void InitRaritiesArray(TRarity[] allRarities)
		{
			AllRarities = allRarities;
		}

		#endregion
		
		#region Get

		/// <summary>
		/// Gets a random loot object.
		/// </summary>
		/// <returns>
		/// An instance of <see cref="TLootObject"/> or default value for its type if no loot data exists.
		/// </returns>
		public TLootObject GetRandomLoot()
		{
			if (!loot.IsNullOrEmpty())
			{
				return GetLootByRarity(new List<TRarity>(AllRarities));
			}
			
			Debug.LogError("{Loot group has no data!}");
			return default;
		}

		/// <summary>
		/// Gets a loot object with specified rarity.
		/// </summary>
		/// <param name="raritiesToInclude">Rarities to include.</param>
		/// <param name="raritiesToExclude">Rarities to exclude..</param>
		/// <returns>
		/// An instance of <see cref="TLootObject"/> or default value for its type if no loot with specified rarity exists.
		/// </returns>
		public TLootObject GetLootByRarity(List<TRarity> raritiesToInclude = default, List<TRarity> raritiesToExclude = default)
		{
			if (loot.IsNullOrEmpty())
			{
				Debug.LogError("{Loot group has no data!}");
				return default;
			}
			
			if (!weightsComputed)
			{
				ComputeWeights();
			}

			List<TRarity> allowedRarities = CreateAllowedRarityList(raritiesToInclude, raritiesToExclude);
			
			List<TLoot> lootOfRarity = GetLootListByRarity(allowedRarities);

			if (lootOfRarity.IsEmpty())
			{
				Debug.LogError($"Loot group has no loot with specified rarities!");
				return default;
			}
			
			if (lootOfRarity.Count == loot.Count)
			{
				return GetWeightedRandomLoot(loot, totalLootWeight);
			}
			
			ComputeWeights(lootOfRarity, out float lootWeight);

			return GetWeightedRandomLoot(lootOfRarity, lootWeight);
		}
		
		/// <summary>
		/// Gets loot entries in this group that match any of the passed loot rarities.
		/// </summary>
		/// <param name="rarities">Allowed loot rarities.</param>
		/// <returns>
		/// A list of <see cref="TLoot"/> objects.v
		/// </returns>
		private List<TLoot> GetLootListByRarity(List<TRarity> rarities)
		{
			if (rarities.Count == AllRarities.Length)
			{
				return loot;
			}
			
			var lootWithRarity = new List<TLoot>();

			foreach (TLoot lootObject in loot)
			{
				if (!rarities.Contains(lootObject.Rarity))
				{
					continue;
				}
				
				lootWithRarity.Add(lootObject);
			}
			
			return lootWithRarity;
		}

		/// <summary>
		/// Gets a random weighted loot object from the passed list.
		/// </summary>
		/// <param name="lootList">Loot list.</param>
		/// <param name="lootTotalWeight">Total weight of items in the list.</param>
		/// <returns>
		/// </returns>
		private TLootObject GetWeightedRandomLoot(List<TLoot> lootList, float lootTotalWeight)
		{
			float index = Random.Range(0, lootTotalWeight);
 
			foreach (var lootObject in lootList)
			{
				if ((index > lootObject.RangeFrom) && (index < lootObject.RangeTo))
				{
					return lootObject.Loot;
				}
			}

			Debug.LogError(("Failed to get weighted loot object!"));
			return lootList.First().Loot;
		}

		/// <summary>
		/// Creates a list of allowed rarities.
		/// </summary>
		/// <param name="raritiesToInclude">Rarities to include in the resulting list.</param>
		/// <param name="raritiesToExclude">Rarities to exclude from the resulting list.</param>
		/// <returns>A list of rarities based on passed arguments.</returns>
		private List<TRarity> CreateAllowedRarityList(List<TRarity> raritiesToInclude = default,
		                                              List<TRarity> raritiesToExclude = default)
		{
			List<TRarity> allowedRarities = new List<TRarity>((raritiesToInclude.IsNullOrEmpty() ? AllRarities : raritiesToInclude)!);

			if (raritiesToExclude != null)
			{
				allowedRarities = allowedRarities.FindAll(rarity => !raritiesToExclude.Contains(rarity));
			}

			return allowedRarities;
		}
		
		#endregion

		#region Weights

		/// <summary>
		/// Computes the weights of the loot in this group.
		/// </summary>
		public void ComputeWeights()
		{
			ComputeWeights(loot, out float totalComputedWeight);
			totalLootWeight = totalComputedWeight;

			weightsComputed = true;
		}

		/// <summary>
		/// Computes the weights of the loot objects in the passed list.
		/// </summary>
		/// <param name="lootList">List of loot.</param>
		/// <param name="totalWeight">Total weight of all loot objects in the list.</param>
		private void ComputeWeights(List<TLoot> lootList, out float totalWeight)
		{
			totalWeight = 0f;
			
			foreach(var lootObject in lootList)
			{
				if(lootObject.Weight >= 0f)
				{
					lootObject.RangeFrom = totalWeight;
					totalWeight += lootObject.Weight;	
					lootObject.RangeTo = totalWeight;
				} 
				else 
				{
					lootObject.Weight =  0f;						
				}
			}
			
			foreach(var group in lootList)
			{
				group.SelectionChance = group.Weight / totalWeight * 100f;
			}
		}

		#endregion
	}
}