using Sirenix.OdinInspector;
using UnityEngine;

namespace Unidork.LootSystem
{
    /// <summary>
    /// Base class for objects storing data about a loot item selected via weighted random.
    /// </summary>
    /// <typeparam name="TObject">Actual loot object.</typeparam>
    /// <typeparam name="TRarity">Loot rarity.</typeparam>
    [System.Serializable]
    public class WeightedLoot<TObject, TRarity> where TRarity : System.Enum
    {
        #region Properties

        /// <summary>
        /// Loot object or value.
        /// </summary>
        /// <value>
        /// Gets the value of the field loot.
        /// </value>
        public TObject Loot => loot;

        /// <summary>
        /// Rarity of this type of loot.
        /// </summary>
        /// <value>
        /// Gets the value of the field rarity.
        /// </value>
        public TRarity Rarity => rarity;

        /// <summary>
        /// Weight of this loot for the purposes of random selection.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the float field weight.
        /// </value>
        public float Weight { get => weight; set => weight = value; }

        /// <summary>
        /// Low bound of this object's weight range.
        /// </summary>
        public float RangeFrom { get; set; }

        /// <summary>
        /// High bound of this object's weight range.
        /// </summary>
        public float RangeTo { get; set; }

        /// <summary>
        /// Chance of this object to be selected randomly.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the float field selectionChance.
        /// </value>
        public float SelectionChance { get => selectionChance; set => selectionChance = value; }

        #endregion

        #region Fields

        /// <summary>
        /// Loot object or value.
        /// </summary>
        [PropertyOrder(2)]
        [Tooltip("Loot object or value.")]
        [SerializeField]
        protected TObject loot;

        /// <summary>
        /// Rarity of this type of loot.
        /// </summary>
        [PropertyOrder(1)]
        [HideIf("@this.IsDerivedClass()")]
        [Tooltip("Rarity of this type of loot.")]
        [SerializeField] 
        private TRarity rarity = default;
        
        /// <summary>
        /// Weight of this loot for the purposes of random selection.
        /// </summary>
        [PropertyOrder(3)]
        [Tooltip("Weight of this loot for the purposes of random selection.")]
        [SerializeField]
        private float weight = 1f;

        /// <summary>
        /// Chance of this object to be selected randomly.
        /// </summary>
        [PropertyOrder(4)]
        [SerializeField, ReadOnly]
        private float selectionChance;
        
        #endregion

#if UNITY_EDITOR
        
        #region Editor

        private bool IsDerivedClass()
        {
            return GetType().IsSubclassOf(typeof(WeightedLoot<TObject, TRarity>));
        }

        #endregion
        
#endif
    }
}