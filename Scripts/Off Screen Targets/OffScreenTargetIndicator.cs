using Unidork.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace Unidork.OffScreenTargets
{
	public class OffScreenTargetIndicator : MonoBehaviour
	{
		#region Properties

		public static Camera MainCamera;
		
		/// <summary>
		/// Target associated with this indicator.
		/// </summary>
		public IOffScreenTarget Target => targetData.Target;

		#endregion
		
		#region Fields
		
		/// <summary>
		/// Indicator's root rect transform.
		/// </summary>
		[Space, ComponentsHeader, Space] 
		[Tooltip("Indicator's root rect transform.")]
		[SerializeField]
		private RectTransform rectTransform = null;

		/// <summary>
		/// Icon that represents an individual target.
		/// </summary>
		[Tooltip("Icon that represents an individual target.")]
		[SerializeField]
		private Image targetIcon = null;

		/// <summary>
		/// Data about target associated with this indicator.
		/// </summary>
		private OffScreenTargetData targetData;

		#endregion

		#region Setup

		/// <summary>
		/// Sets the off screen target data associated with this indicator.
		/// </summary>
		/// <param name="targetData">Target data.</param>
		public void SetData(OffScreenTargetData targetData)
		{
			this.targetData = targetData;

			if (targetIcon == null)
			{
				return;
			}

			targetIcon.sprite = this.targetData.Target.Icon;
		}

		#endregion

		#region Position

		/// <summary>
		/// Positions and rotates the indicator to match the passed data. Keeps individual target icon, if one is active, at zero rotation.
		/// </summary>
		/// <param name="screenPosition">Indicator screen position.</param>
		/// <param name="rotationAngle">Indicator rotation angle.</param>
		public void SetPositionAndRotation(Vector3 screenPosition, float rotationAngle)
		{
			transform.position = screenPosition;
			transform.rotation = Quaternion.Euler(0, 0, rotationAngle * Mathf.Rad2Deg);
			
			if (targetIcon == null)
			{
				return;
			}
			
			targetIcon.transform.rotation = Quaternion.identity;
		}

		#endregion

		#region Icon

		/// <summary>
		/// Enables the icon that represents the type of an individual target.
		/// </summary>
		public void EnableTargetIcon() => targetIcon.enabled = true;

		/// <summary>
		/// Disables the icon that represents the type of an individual target.
		/// </summary>
		public void DisableTargetIcon()
		{
			if (targetIcon == null)
			{
				return;
			}
			
			targetIcon.enabled = false;
		}

		#endregion
	}
}