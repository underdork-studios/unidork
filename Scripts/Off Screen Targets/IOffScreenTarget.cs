using UnityEngine;

namespace Unidork.OffScreenTargets
{
	public interface IOffScreenTarget
	{
		Vector3 Position { get; }
		Sprite Icon { get; }
	}    
}