﻿using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Unidork.GameStates
{
	/// <summary>
	/// Scriptable object that holds current game state.
	/// </summary>
	[CreateAssetMenu(fileName = "Current Game State", menuName = "Game States/Current Game State", order = 1)]
	public class CurrentGameState : ScriptableObject
	{		
		#region Properties

		/// <summary>
		/// Current game state object.
		/// </summary>
		/// <value>
		/// Gets and sets the value of the field value.
		/// </value>
		public GameState Value
		{
			get => value;

			set
			{
				bool switchingToSameState = this.value == value;
				
				if (this.value != null && !switchingToSameState)
				{
					this.value.OnStateExited();
				}

				this.value = value;

				if (this.value != null && !switchingToSameState)
				{
					this.value.OnStateEntered();
				}

				ReactiveValue.Value = value;
			}
		}

		/// <summary>
		/// Reactive properties that other objects can subscribe to to track current game state changes.
		/// </summary>
		public ReactiveProperty<GameState> ReactiveValue { get; private set; }

		#endregion

		#region Fields

		/// <summary>
		/// Current game state object.
		/// </summary>
		[ShowInInspector, ReadOnly]
		private GameState value;

		#endregion

		#region Init

		private void OnEnable()
		{
			value = null;
			ReactiveValue = new ReactiveProperty<GameState>(null);
		}

		#endregion
	}
}