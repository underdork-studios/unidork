﻿using Sirenix.OdinInspector;
using Unidork.Attributes;
using UnityEngine;

namespace Unidork.GameStates
{
    /// <summary>
    /// Switches game states in response to in-game events.
    /// </summary>
    public class GameStateSwitcher : MonoBehaviour
    {
        #region Fields        

        /// <summary>
        /// Array storing all in-game states.
        /// </summary>
        [Space, Sirenix.OdinInspector.Title("STATES", TitleAlignment = TitleAlignments.Centered, HorizontalLine = false), Space]
        [Tooltip("Array storing all in-game states.")]
        [SerializeField]
        private GameState[] gameStates = null;

	    /// <summary>
	    /// Game state to set on start. If left blank, will default to game state with name "Splash".
	    /// </summary>
	    [Tooltip(("Game state to set on start. If left blank, will default to game state with name \"Splash\"."))]
	    [SerializeField]
	    private GameState startState = null;
	    
        /// <summary>
        /// Scriptable object that holds current game state.
        /// </summary>
        [Space, AssetsHeader, Space]
        [Tooltip("Scriptable object that holds current game state.")]
        [SerializeField]
        private CurrentGameState currentGameState = null;

	    /// <summary>
	    /// Should state changes be logged to console?
	    /// </summary>
	    [Space, DebugHeader, Space] 
	    [Tooltip("Should state changes be logged to console?")]
	    [SerializeField]
	    private bool logStateChangesToConsole = false;

		#endregion

		#region Init

		private void Awake()
		{
			if (startState != null)
		    {
			    SwitchToState(startState);
			    return;
		    }
		    
		    SwitchToState("Splash");
	    }

	    #endregion

		#region Switch

		/// <summary>
		/// Switches the current game state to the passed value.
		/// </summary>
		/// <param name="newGameState">New game state.</param>
		public void SwitchToState(GameState newGameState)
		{
            currentGameState.Value = newGameState;

			if (!logStateChangesToConsole)
			{
				return;
			}

			if (newGameState == null)
			{
				return;
			}
			
			Debug.Log($"SWITCHED TO STATE: {newGameState.StateName}");
		}

        /// <summary>
        /// Switches current game state to the state with the passed name.
        /// </summary>
        /// <param name="stateName">State name.</param>
        public void SwitchToState(string stateName)
		{
            SwitchToState(GetStateWithName(stateName));
		}

		#endregion

		#region States

        /// <summary>
        /// Gets the state with the specified name.
        /// </summary>
        /// <param name="gameStateName">State name.</param>
        /// <returns>
        /// Game state from the <see cref="gameStates"/> array whose
        /// name matches the passed value or null if suck state doesn't exist.s
        /// </returns>
        protected GameState GetStateWithName(string gameStateName)
		{
            foreach (GameState gameState in gameStates)
			{
                if (gameState.StateName == gameStateName)
				{
                    return gameState;
				}
			}

            return null;
		}

		#endregion
	}
}