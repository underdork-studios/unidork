﻿using Unidork.Attributes;
using Unidork.Events;
using UnityEngine;

namespace Unidork.GameStates
{
	/// <summary>
	/// Scriptable object that stores data and handles operations with a game state.
	/// </summary>
    [CreateAssetMenu(fileName = "GameState_", menuName = "Game States/New Game State", order = 0)]
    public class GameState : ScriptableObject
    {
		#region Properties

		/// <summary>
		/// Name of the game state.
		/// </summary>
		/// <value>
		/// Gets the value of the string field stateName.
		/// </value>
		public string StateName => stateName;

		#endregion

		#region Fields

		/// <summary>
		/// Name of the game state.
		/// </summary>
		[Space, SettingsHeader, Space]
		[Tooltip("Name of the game state.")]
		[SerializeField]
		private string stateName = null;

		/// <summary>
		/// Optional event to raise when the state is entered.
		/// </summary>
		[Space, EventsHeader, Space]
		[Tooltip("Optional event to raise when the state is entered.")]
		[SerializeField]
		private GameEvent eventToRaiseOnStateEntered = null;

		/// <summary>
		/// Optional event to raise when the state is exited.
		/// </summary>
		[Tooltip("Optional event to raise when the state is exited.")]
		[SerializeField]
		private GameEvent eventToRaiseOnStateExited = null;

		#endregion

		#region State

		/// <summary>
		/// Called when the state is entered.
		/// </summary>
		public void OnStateEntered()
		{
			if (eventToRaiseOnStateEntered != null)
			{
				eventToRaiseOnStateEntered.Raise();
			}
		}

		/// <summary>
		/// Called when the state is exited.
		/// </summary>
		public void OnStateExited()
		{
			if (eventToRaiseOnStateExited != null)
			{
				eventToRaiseOnStateExited.Raise();
			}
		}

		#endregion
	}
}